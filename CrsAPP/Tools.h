#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Tools : NSObject

#pragma mark - text heigth
+ (CGFloat)textHeigth:(NSString*)str width:(CGFloat)width fonts:(UIFont*)font;
+ (CGFloat)textHeigth:(NSString*)str width:(CGFloat)width fontSize:(CGFloat)fontSize;

#pragma mark - text width
+ (CGFloat)textWidth:(NSString*)str height:(CGFloat)height fontSize:(CGFloat)fontSize;
+ (CGFloat)textWidth:(NSString*)str height:(CGFloat)height fonts:(UIFont*)font;

+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end

@interface UIView (Metrics)

@property CGPoint origin;
@property CGFloat height;
@property CGFloat width;

-(CGPoint)centerForSubviews;
-(void)roundToNearestPixelEdges;
-(CGFloat)minX;
-(CGFloat)midX;
-(CGFloat)maxX;
-(CGFloat)minY;
-(CGFloat)midY;
-(CGFloat)maxY;
-(CGSize)size;
-(void)setSize:(CGSize)size;
-(void)setOrigin:(CGPoint)origin;
-(void)setOriginX:(CGFloat)x;
-(void)setOriginY:(CGFloat)y;

@end
