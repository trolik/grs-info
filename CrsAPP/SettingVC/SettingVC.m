//
//  SettingVC.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 14/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "SettingVC.h"
#import "CellTitle.h"
#import "CellSwitch.h"
#import "CellStepper.h"
#import "Request.h"

@interface SettingVC () <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableSettings;

@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.navigationItem setLeftBarButtonItem:Item1];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, [UIImage imageNamed:@"barMenu"].size.width, [UIImage imageNamed:@"barMenu"].size.height);
    [btn setImage:[UIImage imageNamed:@"barMenu"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.navigationItem setRightBarButtonItem:Item];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self setSwitch:2 on:[[NSUserDefaults standardUserDefaults]boolForKey:@"AllPush"]];
    [self setSwitch:3 on:[[NSUserDefaults standardUserDefaults]boolForKey:@"TweeterPush"]];
    [self setSwitch:4 on:[[NSUserDefaults standardUserDefaults]boolForKey:@"NewsPush"]];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"TwetterON"])
    {
        [self setSwitch:9 on:YES];
        
        NSIndexPath *indexText = [NSIndexPath indexPathForItem:10 inSection:0];
        CellStepper *cellUsr = [self.tableSettings cellForRowAtIndexPath:indexText];
        [cellUsr.labelText setText:[NSString stringWithFormat: @"Max. Anzeigedauer %d Tage",(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"TwitterCount"]]];
        
        cellUsr.stepperCell.value = [[NSUserDefaults standardUserDefaults] integerForKey:@"TwitterCount"];
        [cellUsr.stepperCell setEnabled:YES];
    }
    else
    {
        [self setSwitch:9 on:NO];
        
        NSIndexPath *indexText = [NSIndexPath indexPathForItem:10 inSection:0];
        CellStepper *cellUsr = [self.tableSettings cellForRowAtIndexPath:indexText];
        [cellUsr.labelText setText:@"Max. Anzeigedauer 3 Tage"];
        
        cellUsr.stepperCell.value = 3;
        [cellUsr.stepperCell setEnabled:NO];
    }
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"NewsON"])
    {
        [self setSwitch:6 on:YES];
        
        NSIndexPath *indexText = [NSIndexPath indexPathForItem:7 inSection:0];
        CellStepper *cellUsr2 = [self.tableSettings cellForRowAtIndexPath:indexText];
        [cellUsr2.labelText setText:[NSString stringWithFormat: @"Max. Anzeigedauer %d Tage",(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"NewsCount"]]];
        
        cellUsr2.stepperCell.value = [[NSUserDefaults standardUserDefaults] integerForKey:@"NewsCount"];
        [cellUsr2.stepperCell setEnabled:YES];
    }
    else
    {
        [self setSwitch:2 on:NO];
        
        NSIndexPath *indexText = [NSIndexPath indexPathForItem:7 inSection:0];
        CellStepper *cellUsr2 = [self.tableSettings cellForRowAtIndexPath:indexText];
        [cellUsr2.labelText setText:@"Max. Anzeigedauer 3 Tage"];
        
        cellUsr2.stepperCell.value = 3;
        [cellUsr2.stepperCell setEnabled:NO];
    }
}


- (IBAction)ClickStepper:(UIStepper *)sender
{
    NSIndexPath *index = [NSIndexPath indexPathForItem:(int)sender.tag inSection:0];
    CellStepper *cellUsr2 = [self.tableSettings cellForRowAtIndexPath:index];
    
    if (sender.tag == 7)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:[sender value] forKey:@"NewsCount"];
    }
    else if (sender.tag == 10)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:[sender value] forKey:@"TwitterCount"];
    }
    
    [cellUsr2.labelText setText:[NSString stringWithFormat: @"Max. Anzeigedauer %d Tage",(int)[sender value]]];
}

- (IBAction)selectSwitch:(UISwitch *)sender {
    switch ((int)sender.tag) {
        case 1:
        {
            if (sender.on)
            {
                [self setSwitch:3 on:YES];
                [self setSwitch:4 on:YES];
                
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"AllPush"];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"TweeterPush"];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"NewsPush"];
                
                [Request reloadUser:[NSString stringWithFormat:@"http://p280790.mittwaldserver.info/app/info/api/?action=update_ios_settings&token=%@&tweets=1&news=1",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]] reply:^(id reply) {
                    NSLog(@"Good");
                } errorUsr:^{
                    NSLog(@"No Good");
                }];
            } else
            {
                [self setSwitch:3 on:NO];
                [self setSwitch:4 on:NO];
                
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"AllPush"];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"TweeterPush"];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"NewsPush"];
                
                [Request reloadUser:[NSString stringWithFormat:@"http://p280790.mittwaldserver.info/app/info/api/?action=update_ios_settings&token=%@&tweets=0&news=0",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]] reply:^(id reply) {
                    NSLog(@"Good");
                } errorUsr:^{
                    NSLog(@"No Good");
                }];
            }
            break;
        }
        case 2:
        {
            if (sender.on)
            {
                [Request reloadUser:[NSString stringWithFormat:@"http://p280790.mittwaldserver.info/app/info/api/?action=update_ios_settings&token=%@&tweets=1",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]] reply:^(id reply) {
                    NSLog(@"Good");
                } errorUsr:^{
                    NSLog(@"No Good");
                }];
                
                if ([self statusSwitch:4])
                {
                    [self setSwitch:2 on:YES];
                }
                
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"TweeterPush"];
            } else
            {
                [Request reloadUser:[NSString stringWithFormat:@"http://p280790.mittwaldserver.info/app/info/api/?action=update_ios_settings&token=%@&tweets=0",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]] reply:^(id reply) {
                    NSLog(@"Good");
                } errorUsr:^{
                    NSLog(@"No Good");
                }];
                
//                if (![self statusSwitch:4])
//                {
                    [self setSwitch:2 on:NO];
//                }
                
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"TweeterPush"];
            }
            break;
        }
        
        case 3:
        {
            if (sender.on)
            {
                [Request reloadUser:[NSString stringWithFormat:@"http://p280790.mittwaldserver.info/app/info/api/?action=update_ios_settings&token=%@&news=1",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]] reply:^(id reply) {
                    NSLog(@"Good");
                } errorUsr:^{
                    NSLog(@"No Good");
                }];
                
                if ([self statusSwitch:3])
                {
                    [self setSwitch:2 on:YES];
                }
                
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"NewsPush"];
            } else
            {
                [Request reloadUser:[NSString stringWithFormat:@"http://p280790.mittwaldserver.info/app/info/api/?action=update_ios_settings&token=%@&news=0",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]] reply:^(id reply) {
                    NSLog(@"Good");
                } errorUsr:^{
                    NSLog(@"No Good");
                }];
                
//                if (![self statusSwitch:3])
//                {
                    [self setSwitch:2 on:NO];
//                }
                
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"NewsPush"];
            }
            break;
        }
        
        case 4:
        {
            NSIndexPath *index = [NSIndexPath indexPathForItem:7 inSection:0];
            CellStepper *cellUsr2 = [self.tableSettings cellForRowAtIndexPath:index];
            if (sender.on)
            {
                cellUsr2.stepperCell.enabled = YES;
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"NewsON"];
            }
            else
            {
                cellUsr2.stepperCell.enabled = NO;
                [cellUsr2.stepperCell setValue:0];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"NewsON"];
                [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"NewsCount"];
                [cellUsr2.labelText setText:@"Max. Anzeigedauer 0 Tage"];
            }
            break;
        }
        
        case 5:
        {
            NSIndexPath *index = [NSIndexPath indexPathForItem:10 inSection:0];
            CellStepper *cellUsr2 = [self.tableSettings cellForRowAtIndexPath:index];
            if (sender.on)
            {
                cellUsr2.stepperCell.enabled = YES;
                
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"TwetterON"];
            }
            else
            {
                cellUsr2.stepperCell.enabled = NO;
                [cellUsr2.stepperCell setValue:0];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"TwetterON"];
                [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"TwetterCount"];
                [cellUsr2.labelText setText:@"Max. Anzeigedauer 0 Tage"];
            }
            break;
        }
    }
}

-(void)setSwitch:(NSInteger)Num on:(BOOL)on
{
    NSIndexPath *index1 = [NSIndexPath indexPathForItem:Num inSection:0];
    CellSwitch *cellUsr1 = [self.tableSettings cellForRowAtIndexPath:index1];
    
    [cellUsr1.switchCell setOn:on];
}

-(BOOL)statusSwitch:(NSInteger)Num
{
    NSIndexPath *index1 = [NSIndexPath indexPathForItem:Num inSection:0];
    CellSwitch *cellUsr1 = [self.tableSettings cellForRowAtIndexPath:index1];
    
    return cellUsr1.switchCell.on;
}

-(void)clickMenu
{
    [self.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
