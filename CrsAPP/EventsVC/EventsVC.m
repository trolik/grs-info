//
//  EventsVC.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 13/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "EventsVC.h"
#import "Request.h"
#import "CellEvents.h"
#import "FullEventsVC.h"

@interface EventsVC () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableEvents;
@property NSArray *arrTable;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic) NSInteger intPage;

@end

@implementation EventsVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.hidesBackButton = YES;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.navigationItem setLeftBarButtonItem:Item1];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, [UIImage imageNamed:@"barMenu"].size.width, [UIImage imageNamed:@"barMenu"].size.height);
    [btn setImage:[UIImage imageNamed:@"barMenu"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.navigationItem setRightBarButtonItem:Item];
    
    __weak typeof(self) weakSelf = self;
    [Request reloadUser:@"http://www.grs.de/api/events-archive.jsonp" reply:^(id reply) {
        weakSelf.arrTable = reply;
        [self.tableEvents reloadData];
    } errorUsr:^{
        NSLog(@"error load data");
    }];
    
    self.tableEvents.estimatedRowHeight = 44;
    self.tableEvents.rowHeight = UITableViewAutomaticDimension;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor colorWithRed:0.29 green:0.381 blue:0.491 alpha:1];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestLoans)
                  forControlEvents:UIControlEventAllEvents];
    
    [self.tableEvents addSubview:self.refreshControl];
    
    self.intPage = 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrTable count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.arrTable count] - 1)
    {
        [self getOldNews];
    }
    
    CellEvents *cell = (CellEvents*)[tableView dequeueReusableCellWithIdentifier:@"CellEvents" forIndexPath:indexPath];
    
    NSString *datStart = [self topos:self.arrTable[indexPath.row][@"Start"] to:@">" pos:@"</"];
    NSString *dataEnd = [self topos:self.arrTable[indexPath.row][@"end"] to:@">" pos:@"</"];
    
    NSMutableAttributedString *strVon = [self addAttribute:@"von: " font:[UIFont boldSystemFontOfSize:12] color:[UIColor colorWithRed:0.29 green:0.381 blue:0.491 alpha:1]];
    [strVon appendAttributedString:[self addAttribute:datStart font:[UIFont systemFontOfSize:12] color:[UIColor colorWithRed:0.29 green:0.381 blue:0.491 alpha:1]]];
    [strVon appendAttributedString:[self addAttribute:@" bis: " font:[UIFont boldSystemFontOfSize:12] color:[UIColor colorWithRed:0.29 green:0.381 blue:0.491 alpha:1]]];
    [strVon appendAttributedString:[self addAttribute:dataEnd font:[UIFont systemFontOfSize:12] color:[UIColor colorWithRed:0.29 green:0.381 blue:0.491 alpha:1]]];
    cell.labelDate.attributedText = strVon;
//    [cell.labelDate setText:[NSString stringWithFormat:@"von: %@ bis: %@",datStart,dataEnd]];
    [cell.labelTitle setText: self.arrTable[indexPath.row][@"node_title"]];
    
    return cell;
}

-(NSString*)topos:(NSString*)str to:(NSString*)to pos:(NSString*)pos
{
    NSRange rangeTo = [str rangeOfString:to];
    NSRange rangePos = [str rangeOfString:pos];
    NSString *subStr = [str substringWithRange:NSMakeRange(rangeTo.location + [to length], rangePos.location - rangeTo.location-1)];
    
    return subStr;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableEvents];
    NSIndexPath *indexPath = [self.tableEvents indexPathForRowAtPoint:hitPoint];
    
    FullEventsVC *vc = segue.destinationViewController;
    
    vc.strTitle = self.arrTable[indexPath.row][@"node_title"];
    vc.strText = self.arrTable[indexPath.row][@"body_1"];
    vc.strDate = ((CellEvents*)sender).labelDate.text;
    vc.strLinks = self.arrTable[indexPath.row][@"Links"];
}

-(void)getOldNews
{
    __weak typeof(self) weakSelf = self;
    self.intPage++;
    [Request reloadUser:[NSString stringWithFormat:@"http://www.grs.de/api/events-archive.jsonp?page=%d",(int)self.intPage] reply:^(id reply) {
        NSMutableArray *arr = [NSMutableArray arrayWithArray: weakSelf.arrTable];
        [arr addObjectsFromArray:reply];
        
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arr];
        NSArray *arrayWithoutDuplicates = [orderedSet array];
        
        weakSelf.arrTable = arrayWithoutDuplicates;
        [weakSelf.tableEvents reloadData];
    } errorUsr:^{
        NSLog(@"error load data");
    }];
}

-(void)getLatestLoans
{
    __weak typeof(self) weakSelf = self;
    [Request reloadUser:@"http://www.grs.de/api/events-archive.jsonp" reply:^(id reply) {
        weakSelf.arrTable = reply;
        [weakSelf.tableEvents reloadData];
        
        [weakSelf.refreshControl endRefreshing];
    } errorUsr:^{
        NSLog(@"error load data");
    }];
    
}

-(void)clickMenu
{
    [self.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}

-(NSMutableAttributedString*)addAttribute:(NSString*)str font:(UIFont*)font1 color:(UIColor*)color
{
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: font1 forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString1 = [[NSMutableAttributedString alloc] initWithString:str attributes: arialDict];
    
    return aAttrString1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}



@end
