//
//  CellEvents.h
//  CrsAPP
//
//  Created by Сергей Лазаренко on 13/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellEvents : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelDate;

@end
