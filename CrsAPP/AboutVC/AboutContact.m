//
//  AboutContact.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 21/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "AboutContact.h"

@interface AboutContact ()

@end

@implementation AboutContact

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callPhone:(id)sender {
    NSString *phNo = @"+49022120680";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (IBAction)callPhone2:(id)sender {
    NSString *phNo = @"+49022120689946";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}


- (IBAction)sendMail:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:info@grs.de"]];
}

@end
