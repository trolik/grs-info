//
//  AboutFirst.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 16/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "AboutFirst.h"

@interface AboutFirst ()
@property (strong, nonatomic) IBOutlet UIWebView *webViewAbout;

@end

@implementation AboutFirst

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.29 green:0.38 blue:0.49 alpha:1]];

    // Do any additional setup after loading the view.
    self.tabBarController.navigationItem.hidesBackButton = YES;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.tabBarController.navigationItem setLeftBarButtonItem:Item1];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, [UIImage imageNamed:@"barMenu"].size.width, [UIImage imageNamed:@"barMenu"].size.height);
    [btn setImage:[UIImage imageNamed:@"barMenu"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.tabBarController.navigationItem setRightBarButtonItem:Item];
    
    NSString *textHtml;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"header_about" ofType:@"png"];
    
    textHtml = @"";
    textHtml = [textHtml stringByAppendingString: [NSString stringWithFormat: @"<style>p { font-family: 'RobotoCondensed-Regular' } img { width: %f; }</style><FONT COLOR=\"#4A617D\"><img src=\"file://%@\">",self.view.frame.size.width-9,path]];
    textHtml = [textHtml stringByAppendingString:@"<div> <p>Die Gesellschaft für Anlagen- und Reaktorsicherheit (GRS) ist eine gemeinnützige technisch-wissenschaftliche Forschungs- und Sachverständigenorganisation. Sie verfügt über interdisziplinäres Wissen, fortschrittliche Methoden und qualifizierte Daten, um die Sicherheit technischer Anlagen zu bewerten und weiterzuentwickeln.</p> <p>Die GRS hat ihren Geschäftsbetrieb im Januar 1977 aufgenommen. Sitz der Gesellschaft ist Köln, weitere Standorte sind Berlin, Braunschweig und Garching bei München. Derzeit beschäftigt die GRS rund 450 Mitarbeiter, davon über 350 im technisch-wissenschaftlichen Bereich. Die GRS finanziert sich ausschließlich über Aufträge. Das jährliche Auftragsvolumen liegt derzeit bei rund 57 Millionen Euro.</p> <p><strong>Auftraggeber</strong><br> Hauptauftraggeber der GRS sind<br> •&nbsp;das Bundesministerium für Umwelt, Naturschutz, Bau und Reaktorsicherheit (BMUB),<br> •&nbsp;das Bundesministerium für Wirtschaft und Technologie (BMWi),<br> •&nbsp;das Bundesministerium für Bildung und Forschung (BMBF),<br> •&nbsp;das Auswärtige Amt sowie<br> •&nbsp;das Bundesamt für Strahlenschutz (BfS).</p> <p>Weiterhin bearbeitet die GRS Aufträge von Landesbehörden, dem Umweltbundesamt und den Technischen Überwachungsvereinen. Wichtigster internationaler Auftraggeber ist die Europäische Kommission.</p> <p><strong>Gesellschafter und Organe</strong><br> Die GRS gehört zu 46 % der Bundesrepublik Deutschland und zu 46 % den Technischen Überwachungs-Vereinen (TÜV) und dem Germanischen Lloyd. Jeweils 4 % der Anteile der GRS haben das Land Nordrhein-Westfalen und der Freistaat Bayern inne.</p> <p>Die Organe der GRS sind die Gesellschafterversammlung, der Aufsichtsrat und die Geschäftsführung der GRS. Vorsitzende des Aufsichtsrats ist die Parlamentarische Staatssekretärin&nbsp;Rita Schwarzelühr-Sutter&nbsp;(BMUB).</p>  </div>"];
    
    
    
    [self.webViewAbout loadHTMLString:textHtml baseURL:nil];
}


-(void)clickMenu
{
    [self.tabBarController.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
