//
//  AboutSecond.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 16/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "AboutSecond.h"
#import <CoreLocation/CoreLocation.h>

@import MapKit;

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface AboutSecond () <UIWebViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) CLLocation *coord;
@property (strong, nonatomic) NSArray *arrCoord;

@end

@implementation AboutSecond

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.locationManager.delegate = self;
    }
    
    if(IS_OS_8_OR_LATER)
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];

    
    self.arrCoord = @[@[@"GRS - Schwertnergasse 1, 50667 Köln, Deutschland",@(50.938691),@(6.952177)],
                     @[@"GRS - Theodor-Heuss-Straße 4, 38122 Braunschweig, Deutschland", @(52.253914),@(10.518145)],
                     @[@"GRS - Kurfürstendamm 200, 10719 Berlin, Deutschland", @(52.501383),@(13.320279)],
                     @[@"GRS - Boltzmannstraße 15, 80747 Garching, Deutschland",@(48.265667),@(11.669301)]];
    
    
    MKCoordinateRegion region = MKCoordinateRegionMake(CLLocationCoordinate2DMake([self.arrCoord[0][1] floatValue],
                                                               [self.arrCoord[0][2] floatValue]), MKCoordinateSpanMake(6, 6));
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:region];
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    for (int i=0; i < [self.arrCoord count]; i++)
    {
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake([self.arrCoord[i][1] floatValue],
                                                                             [self.arrCoord[i][2] floatValue]);
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:centerCoordinate];
        [annotation setTitle:self.arrCoord[i][0]]; //You can set the subtitle too
        [self.mapView addAnnotation:annotation];
    }
    
}

- (IBAction)clickBtn:(UIButton*)sender {
    CLLocationCoordinate2D coordinate = self.coord.coordinate;
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:@"meine aktuelle position"];
    
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};

    
    CLLocationCoordinate2D coordinate2 = CLLocationCoordinate2DMake([self.arrCoord[sender.tag][1] floatValue],
                                                                    [self.arrCoord[sender.tag][2] floatValue]);
    MKPlacemark *placemark2 = [[MKPlacemark alloc] initWithCoordinate:coordinate2
                                                   addressDictionary:nil];
    MKMapItem *currentLocationMapItem = [[MKMapItem alloc] initWithPlacemark:placemark2];
    [currentLocationMapItem setName:self.arrCoord[sender.tag][0]];
    
    [MKMapItem openMapsWithItems:@[mapItem, currentLocationMapItem]
                   launchOptions:launchOptions];
     
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    self.coord = locations[0];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Error %@",error);
}

-(void)dealloc
{
    [self.locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
