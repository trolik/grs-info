//
//  CollectionLayout.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 23/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "CollectionLayout.h"

@implementation CollectionLayout

- (CGSize)collectionViewContentSize
{
    if(([[UIDevice currentDevice] orientation] == 3) ||
       ([[UIDevice currentDevice] orientation] == 4))
    {
        return CGSizeMake(self.collectionView.bounds.size.width, 162*2);
    }
    else
    {
        return CGSizeMake(self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    }
}


- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    CGRect oldBounds = self.collectionView.bounds;
    if (CGRectGetWidth(newBounds) != CGRectGetWidth(oldBounds)) {
        return YES;
    }
    return NO;
}

-(NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    
    for (int i=0;i<6;i++)
    {
        UIEdgeInsets insets = UIEdgeInsetsZero;
        
        CGRect frame = [self rectElement:i];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        
        UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        attributes.frame = UIEdgeInsetsInsetRect(frame, insets);
        
        [arr addObject:attributes];
    }
    
    [arr addObject:[self lineAttribute]];
    
    return arr;
}

-(CGRect)rectElement:(int)num
{
    CGRect frame;
    
    if(([[UIDevice currentDevice] orientation] == 3) ||
       ([[UIDevice currentDevice] orientation] == 4))
    {
        BOOL first = (num < 3 ? YES : NO);
        
        CGFloat hCell = (self.collectionView.frame.size.height-80)/2;
        NSInteger inn = 0;
        
        if (num==0 || num==3) inn = (hCell+10) * 1.5;
        if (num==1  || num==4) inn = (hCell+10) * 0.5;
        if (num==2  || num==5) inn = (hCell+10) * -0.5;
        
        if (first)
        {
            frame = CGRectMake((self.collectionView.frame.size.width/2) - inn, 10, hCell, hCell);
        }
        else
        {
            frame = CGRectMake(self.collectionView.frame.size.width/2 - inn, hCell+20, hCell, hCell);
        }
    }
    else
    {
        BOOL first = (num%2==0?YES:NO);
                
        CGFloat floatWidth = (self.collectionView.frame.size.width/2) - ((self.collectionView.frame.size.width/2)*0.14);
        
        if (first)
        {
            frame = CGRectMake(self.collectionView.frame.size.width/2-(floatWidth+5), (num/2*(floatWidth+10))+10, floatWidth, floatWidth);
        }
        else
        {
            frame = CGRectMake(self.collectionView.frame.size.width/2+5, (num/2*(floatWidth+10))+10, floatWidth, floatWidth);
        }
    }
    
    return frame;
}

-(UICollectionViewLayoutAttributes*)lineAttribute
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:6 inSection:0];
    
    UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    attributes.frame = UIEdgeInsetsInsetRect(CGRectMake(0,
                                                        self.collectionView.frame.size.height-50,
                                                        self.collectionView.frame.size.width,
                                                        50), insets);
    
    return attributes;
}


@end
