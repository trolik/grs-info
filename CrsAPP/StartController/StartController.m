//
//  StartController.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 20/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "StartController.h"
#import "ViewController.h"
#import "Request.h"

@interface StartController ()

@end

@implementation StartController

- (void)viewDidLoad {
    [super viewDidLoad];

    [Request loadAllLinks:^(id reply) {
        [self performSegueWithIdentifier:@"startVC" sender:reply];
    } errorUsr:^{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }];
}




-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ViewController *vc = [segue destinationViewController];
    
    [vc setDictAll: [[NSDictionary alloc]initWithDictionary: sender]];
    
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
