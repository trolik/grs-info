#import "Tools.h"

@implementation Tools


/////////////работа со шрифтами
+(CGFloat)textHeigth:(NSString*)str width:(CGFloat)width fonts:(UIFont*)font
{
    CGSize constraintSize = CGSizeMake(width, MAXFLOAT);
    str = !str ? @"" : str;
    NSMutableAttributedString *atext = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:font}];
    CGRect arect = [atext boundingRectWithSize:constraintSize options: NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return arect.size.height;
}

+(CGFloat)textHeigth:(NSString*)str width:(CGFloat)width fontSize:(CGFloat)fontSize
{
    CGSize constraintSize = CGSizeMake(width, MAXFLOAT);
    NSMutableAttributedString *atext = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
    CGRect arect = [atext boundingRectWithSize:constraintSize options: NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return arect.size.height;
}

+(CGFloat)textWidth:(NSString*)str height:(CGFloat)height fonts:(UIFont*)font
{
    CGSize constraintSize = CGSizeMake(MAXFLOAT,height);
    NSMutableAttributedString *atext = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:font}];
    CGRect arect = [atext boundingRectWithSize:constraintSize options: NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return arect.size.height;
}

+(CGFloat)textWidth:(NSString*)str height:(CGFloat)height fontSize:(CGFloat)fontSize
{
    CGSize constraintSize = CGSizeMake(MAXFLOAT,height);
    NSMutableAttributedString *atext = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
    CGRect arect = [atext boundingRectWithSize:constraintSize options: NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return arect.size.height;
}

/////////////////////////////////


+(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end

@implementation UIView (Metrics)

-(CGPoint)centerForSubviews{
    return CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
}

-(void)setWidth:(CGFloat)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

-(void)setHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

-(CGFloat)width{
    return CGRectGetWidth(self.bounds);
}

-(CGFloat)height{
    return CGRectGetHeight(self.bounds);
}

-(void)roundToNearestPixelEdges{
    CGRect frame = self.frame;
    frame.origin = CGPointMake(roundf(frame.origin.x), roundf(frame.origin.y));
    frame.size = CGSizeMake(roundf(frame.size.width), roundf(frame.size.height));
    self.frame = frame;
}

-(void)setSize:(CGSize)size{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

-(void)setOrigin:(CGPoint)origin{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

-(void)setOriginX:(CGFloat)x{
    CGRect frame = self.frame;
    frame.origin = CGPointMake(x, frame.origin.y);
    self.frame = frame;
}

-(void)setOriginY:(CGFloat)y{
    CGRect frame = self.frame;
    frame.origin = CGPointMake(frame.origin.y, y);
    self.frame = frame;
}

-(CGPoint)origin{
    return self.frame.origin;
}

-(CGFloat)minX{
    return CGRectGetMinX(self.frame);
}

-(CGFloat)midX{
    return CGRectGetMidX(self.frame);
}

-(CGFloat)maxX{
    return CGRectGetMaxX(self.frame);
}

-(CGFloat)minY{
    return CGRectGetMinY(self.frame);
}

-(CGFloat)midY{
    return CGRectGetMidY(self.frame);
}

-(CGFloat)maxY{
    return CGRectGetMaxY(self.frame);
}

-(CGSize)size{
    return self.bounds.size;
}

@end
