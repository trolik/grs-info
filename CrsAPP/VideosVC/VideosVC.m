//
//  VideosVC.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 13/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "VideosVC.h"
#import "Request.h"
#import "CellVideos.h"
#import <sys/utsname.h>

@interface VideosVC ()

@property NSArray *arrTable;

@property (strong, nonatomic) IBOutlet UIWebView *webViewVideo;

@end

@implementation VideosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.navigationItem setLeftBarButtonItem:Item1];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, [UIImage imageNamed:@"barMenu"].size.width, [UIImage imageNamed:@"barMenu"].size.height);
    [btn setImage:[UIImage imageNamed:@"barMenu"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.navigationItem setRightBarButtonItem:Item];
    
    __weak typeof(self) weakSelf = self;
    [Request reloadUser:@"http://www.grs.de/api/video-app.jsonp" reply:^(id reply) {
        
        NSLog(@"%@",[[UIDevice currentDevice] model]);
        
        NSString *textHtml = @"<style>a { color: #D41D0E; } iframe { padding: 0px 0px 0px 2px; } p:first-child {  } p{ color:#4A617D; font-family: 'RobotoCondensed-Regular'; } </style>";
        textHtml = [textHtml stringByAppendingString:reply[0][@"body"]];

        textHtml = [textHtml stringByReplacingOccurrencesOfString:@"src=\"//" withString:@"src=\"http://"];
        textHtml = [textHtml stringByReplacingOccurrencesOfString:@"href=\"/" withString:@"href=\"http://www.grs.de/"];
        
        if ([[self deviceName]rangeOfString:@"iPhone7,1"].location != NSNotFound &&
            [[self deviceName]rangeOfString:@"iPhone8,2"].location != NSNotFound)
        {
            if (self.view.frame.size.width < self.view.frame.size.height)
                textHtml = [textHtml stringByReplacingOccurrencesOfString:@"width=\"515" withString:[NSString stringWithFormat: @"width=\"%d",(int)self.view.frame.size.width - 22]];
            else
                textHtml = [textHtml stringByReplacingOccurrencesOfString:@"width=\"515" withString:[NSString stringWithFormat: @"width=\"%d",(int)self.view.frame.size.height - 22]];
        }
        else
        {
            if (self.view.frame.size.width < self.view.frame.size.height)
                textHtml = [textHtml stringByReplacingOccurrencesOfString:@"width=\"515" withString:[NSString stringWithFormat: @"width=\"%d",(int)self.view.frame.size.width - 12]];
            else
                textHtml = [textHtml stringByReplacingOccurrencesOfString:@"width=\"515" withString:[NSString stringWithFormat: @"width=\"%d",(int)self.view.frame.size.height - 12]];
        }
        [weakSelf.webViewVideo loadHTMLString:textHtml baseURL:nil] ;
        
    } errorUsr:^{
        NSLog(@"error load data");
    }];
}

-(NSString*) deviceName;
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

-(void)clickMenu
{
    [self.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
