//
//  TwitterVC.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 13/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "TwitterVC.h"
#import "Request.h"
#import "TwitterCell.h"
#import "Tools.h"
#import "FullWebView.h"

@interface TwitterVC ()
@property (strong, nonatomic) IBOutlet UITableView *tableTwitter;


@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic) NSInteger intPage;

@end

@implementation TwitterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.navigationItem setLeftBarButtonItem:Item1];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, [UIImage imageNamed:@"barMenu"].size.width, [UIImage imageNamed:@"barMenu"].size.height);
    [btn setImage:[UIImage imageNamed:@"barMenu"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.navigationItem setRightBarButtonItem:Item];

    
    self.tableTwitter.estimatedRowHeight = 44;
    self.tableTwitter.rowHeight = UITableViewAutomaticDimension;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor colorWithRed:0.29 green:0.381 blue:0.491 alpha:1];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestLoans)
                  forControlEvents:UIControlEventAllEvents];
    
    [self.tableTwitter addSubview:self.refreshControl];
    
    self.intPage = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrTable count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TwitterCell *cell;
    
    if (self.arrTable[indexPath.row][@"entities"][@"media"][0][@"media_url"] == nil)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellTwitter" forIndexPath:indexPath];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellTwitterMedia" forIndexPath:indexPath];
        
        __weak typeof(self) weakSelf = self;
        [Request operationWihtRequestString:self.arrTable[indexPath.row][@"entities"][@"media"][0][@"media_url"] сompletion:^(BOOL sucess, NSData *recievedData)
         {
             NSIndexPath *indPath = [weakSelf.tableTwitter indexPathForCell:cell];
             
             if (indexPath != indPath)
             {
                 return;
             }
             
             UIImage *img = [[UIImage alloc] initWithData:recievedData];
             
             if (!img)
             {
                 return;
             }
             
             [cell.imgMedia setImage:img];
         }];
    }
    
    NSString *name = self.arrTable[indexPath.row][@"user"][@"name"];
    NSString *screenName = self.arrTable[indexPath.row][@"user"][@"screen_name"];
    UIFont *font1 = [UIFont boldSystemFontOfSize:17];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: font1 forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString1 = [[NSMutableAttributedString alloc] initWithString:name attributes: arialDict];
    UIFont *font2 = [UIFont systemFontOfSize:12];
    NSDictionary *arialDict2 = [NSDictionary dictionaryWithObject: font2 forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString2 = [[NSMutableAttributedString alloc] initWithString:[@" " stringByAppendingString:screenName] attributes: arialDict2];
    
    [aAttrString1 appendAttributedString:aAttrString2];
    cell.labelNick.attributedText = aAttrString1;
    
    [cell.labelText setText: self.arrTable[indexPath.row][@"text"]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE MMM dd HH:mm:ss zzz yyyy"];
    NSString *dateString = self.arrTable[indexPath.row][@"created_at"];
    NSDate *dateTime = [dateFormat dateFromString:dateString];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    [cell.labelDate setText:[dateFormat stringFromDate:dateTime]];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"TwetterON"] &&
        [[NSUserDefaults standardUserDefaults]integerForKey:@"TwitterCount"] > indexPath.row)
    {
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"arrTwetts"]containsObject:self.arrTable[indexPath.row][@"id_str"]])
        {
            [cell setBackgroundColor:[UIColor colorWithRed:0.623 green:0.69 blue:0.781 alpha:1]];
        }
        else
        {
            [cell setBackgroundColor:[UIColor clearColor]];
        }

    }
    else
    {
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    
    [cell.avatar.layer setCornerRadius:10];
    __weak typeof(self) weakSelf = self;
    [Request operationWihtRequestString:self.arrTable[indexPath.row][@"user"][@"profile_image_url"] сompletion:^(BOOL sucess, NSData *recievedData)
     {
         NSIndexPath *indPath = [weakSelf.tableTwitter indexPathForCell:cell];
         
         if (indexPath != indPath)
         {
             return;
         }
         
         UIImage *img = [[UIImage alloc] initWithData:recievedData];
         
         if (!img)
         {
             return;
         }
         
         [cell.avatar setImage:img];
     }];
    
    return cell;
}

-(void)getLatestLoans
{
    __weak typeof(self) weakSelf = self;
    [Request reloadUser:@"http://p280790.mittwaldserver.info/app/info/data/tweets_json.php?count=250&page=1" reply:^(id reply) {
        weakSelf.arrTable = reply;
        [weakSelf.tableTwitter reloadData];
        
        [weakSelf.refreshControl endRefreshing];
    } errorUsr:^{
        NSLog(@"error load data");
    }];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray: [[NSUserDefaults standardUserDefaults]arrayForKey:@"arrTwetts"]];
    [arr addObject:self.arrTable[indexPath.row][@"id_str"]];
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:@"arrTwetts"];
    
    
    NSString *link = [NSString stringWithFormat:@"https://twitter.com/%@/status/%@",
                      self.arrTable[indexPath.row][@"user"][@"screen_name"],
                      self.arrTable[indexPath.row][@"id_str"]];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:link]];
    
    [self performSegueWithIdentifier:@"openFullBrowser" sender:request];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    FullWebView *vc = [segue destinationViewController];
    vc.request = sender;
}

-(void)clickMenu
{
    [self.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}





@end
