//
//  TwitterCellMedia.h
//  CrsAPP
//
//  Created by Сергей Лазаренко on 15/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwitterCellMedia : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (strong, nonatomic) IBOutlet UILabel *labelNick;
@property (strong, nonatomic) IBOutlet UILabel *labelDate;
@property (strong, nonatomic) IBOutlet UILabel *labelText;
@property (strong, nonatomic) IBOutlet UIImageView *imgMedia;

@end
