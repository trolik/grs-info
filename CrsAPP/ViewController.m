//
//  ViewController.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 11/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "ViewController.h"
#import "CellCollection.h"
#import "NewsVC.h"
#import "TwitterVC.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property NSArray *arrColor;
@property NSArray *arrName;
@property NSArray *arrImg;
@property (strong, nonatomic) IBOutlet UICollectionView *CollView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setHidden:NO];
    
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"StartUp"])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"TwitterCount"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TwetterON"];
        
        [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"NewsCount"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NewsON"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"StartUp"];
    }
    
    // Do any additional setup after loading the view, typically from a nib.
    self.arrColor = @[[UIColor colorWithRed:0.721 green:0.113 blue:0.114 alpha:1],
                      [UIColor colorWithRed:0.247 green:0.6 blue:1 alpha:1],
                      [UIColor colorWithRed:0 green:0.59 blue:0.25 alpha:1],
                      [UIColor colorWithRed:0.953 green:0.572 blue:0 alpha:1],
                      [UIColor colorWithRed:0.241 green:0.316 blue:0.71 alpha:1],
                      [UIColor colorWithRed:0.21 green:0.278 blue:0.31 alpha:1]];
    
    self.arrName = @[@"Aktuelles",@"Tweets",@"Veranstaltungen",@"Karriere",@"Videos",@"Über uns"];
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.navigationItem setLeftBarButtonItem:Item1];
    
    //    [[NSUserDefaults standardUserDefaults]objectForKey:@"arrNews"]
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.CollView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.CollView reloadData];
}

#pragma mark - collectionview DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 7;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CellCollection *cell;
    
    if (indexPath.row != 6)
    {
        cell = (CellCollection*)[collectionView dequeueReusableCellWithReuseIdentifier:@"CellNews" forIndexPath:indexPath];
        
        [cell setBackgroundColor:self.arrColor[indexPath.row]];
        [cell.labelCell setText:self.arrName[indexPath.row]];
        
        [cell.imgCell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon%d",(int)indexPath.row + 1]]];
        
        if (indexPath.row == 0)
        {
            NSInteger numCount = [self countNewNews];
            if (numCount > 0)
            {
                [cell.numView setHidden:NO];
                [cell.labelCount setText:[NSString stringWithFormat:@"%d",(int)numCount]];
            }
        } else if (indexPath.row == 1)
        {
            NSInteger numCount = [self countNewTweets];
            if (numCount > 0)
            {
                [cell.numView setHidden:NO];
                [cell.labelCount setText:[NSString stringWithFormat:@"%d",(int)numCount]];
            }
        }
        else
        {
            [cell.numView setHidden:YES];
        }
        
        cell.layer.cornerRadius = 5;
    }
    else
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellLink" forIndexPath:indexPath];
    }
    
    return cell;
}

#pragma mark - collectionview Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize retval;
    
    if(([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight) || ([[UIDevice currentDevice] orientation] == UIInterfaceOrientationLandscapeLeft))
    {
        // do something for Landscape mode
        retval = CGSizeMake(self.view.frame.size.height/2-40, self.view.frame.size.height/2-45);
    }
    else
    {
        retval = CGSizeMake(self.view.frame.size.width/2-40, self.view.frame.size.width/2-45);
    }
    
    return retval;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        [self performSegueWithIdentifier:@"performNews" sender: indexPath];
    }
    else if (indexPath.row == 1)
    {
        [self performSegueWithIdentifier:@"performTweeter" sender: indexPath];
    }
    else if (indexPath.row == 2)
    {
        [self performSegueWithIdentifier:@"performEvents" sender:indexPath];
    }
    else if (indexPath.row == 3)
    {
        [self performSegueWithIdentifier:@"performCareer" sender:indexPath];
    }
    else if (indexPath.row == 4)
    {
        [self performSegueWithIdentifier:@"performVideos" sender:indexPath];
    }
    else if (indexPath.row == 5)
    {
        [self performSegueWithIdentifier:@"performSettings" sender:indexPath];
    }
    else if (indexPath.row == 6)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://Grs.de"]];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender class] == [NSIndexPath class])
    {
        if (((NSIndexPath*)sender).row == 0)
        {
            NewsVC *vc = [segue destinationViewController];
            vc.arrTable = self.dictAll[@"News"];
        }
        else if (((NSIndexPath*)sender).row == 1) {
            TwitterVC *vc = [segue destinationViewController];
            vc.arrTable = self.dictAll[@"Tweets"];
        }
    }
}

-(NSInteger)countNewNews
{
    NSArray *arrNews = [[NSUserDefaults standardUserDefaults]objectForKey:@"arrNews"];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"NewsON"])
    {
        if (arrNews == nil)
        {
            return [[NSUserDefaults standardUserDefaults]integerForKey:@"NewsCount"];
        }
        else
        {
            NSInteger selNews = [[NSUserDefaults standardUserDefaults]integerForKey:@"NewsCount"];
            NSInteger numCount = 0;
            
            for (int i = 0; i < selNews; i++) {
                if (![arrNews containsObject:self.dictAll[@"News"][i][@"nid"]])
                {
                    numCount++;
                }
            }
            
            if (numCount == selNews) [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arrNews"];
            
            return numCount;
        }
    }
    else
        return 0;
}

-(NSInteger)countNewTweets
{
    NSArray *arrNews = [[NSUserDefaults standardUserDefaults]objectForKey:@"arrTwetts"];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"TwetterON"]) {
        if (arrNews == nil)
        {
            return [[NSUserDefaults standardUserDefaults]integerForKey:@"TwitterCount"];
        }
        else
        {
            NSInteger selNews = [[NSUserDefaults standardUserDefaults]integerForKey:@"TwitterCount"];
            NSInteger numCount = 0;
            
            for (int i = 0; i < selNews; i++) {
                if (![arrNews containsObject:self.dictAll[@"Tweets"][i][@"id_str"]])
                {
                    numCount++;
                }
            }
            
            if (numCount == selNews) [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arrTwetts"];
            
            return numCount;
        }
    }
    else
        return 0;
}

@end
