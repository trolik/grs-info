//
//  NewsVC.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 11/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "NewsVC.h"
#import "Request.h"
#import "CellNews.h"
#import "Tools.h"
#import "FullNewsVC.h"

@interface NewsVC () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic) NSInteger intPage;

@end

@implementation NewsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.navigationItem setLeftBarButtonItem:Item1];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, [UIImage imageNamed:@"barMenu"].size.width, [UIImage imageNamed:@"barMenu"].size.height);
    [btn setImage:[UIImage imageNamed:@"barMenu"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.navigationItem setRightBarButtonItem:Item];
    
//    __weak typeof(self) weakSelf = self;
//    [Request reloadUser:@"http://www.grs.de/api/news-archive.jsonp" reply:^(id reply) {
//        weakSelf.arrTable = reply;
//        [weakSelf.table reloadData];
//    } errorUsr:^{
//        NSLog(@"error load data");
//    }];
    
    self.table.estimatedRowHeight = 44;
    self.table.rowHeight = UITableViewAutomaticDimension;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor colorWithRed:0.29 green:0.381 blue:0.491 alpha:1];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestLoans)
                  forControlEvents:UIControlEventAllEvents];
    
    [self.table addSubview:self.refreshControl];

    self.intPage = 0;
}


-(void)clickMenu
{
    [self.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrTable count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.arrTable count] - 1)
    {
        [self getOldNews];
    }
    
    CellNews *cell = (CellNews*)[tableView dequeueReusableCellWithIdentifier:@"CellNews" forIndexPath:indexPath];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"NewsON"] &&
        [[NSUserDefaults standardUserDefaults]integerForKey:@"NewsCount"] > indexPath.row)
    {
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"arrNews"]containsObject:self.arrTable[indexPath.row][@"nid"]])
        {
            [cell setBackgroundColor:[UIColor colorWithRed:0.623 green:0.69 blue:0.781 alpha:1]];
        }
        else
        {
            [cell setBackgroundColor:[UIColor clearColor]];
        }
    }
    else
    {
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSString *dateString = self.arrTable[indexPath.row][@"Datum"][0];
    NSDate *dateTime = [dateFormat dateFromString:dateString];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    [cell.labelDate setText:[dateFormat stringFromDate:dateTime]];
    
    [cell.labelTitle setText: self.arrTable[indexPath.row][@"node_title"]];
    
    [cell.labelTiser setText: self.arrTable[indexPath.row][@"teaser"]];
    
    return cell;
}


-(void)getOldNews
{
    __weak typeof(self) weakSelf = self;
    self.intPage++;
    [Request reloadUser:[NSString stringWithFormat:@"http://www.grs.de/api/news-archive.jsonp?page=%d",(int)self.intPage] reply:^(id reply) {
        NSMutableArray *arr = [NSMutableArray arrayWithArray: weakSelf.arrTable];
        [arr addObjectsFromArray:reply];
        
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arr];
        NSArray *arrayWithoutDuplicates = [orderedSet array];
        
        weakSelf.arrTable = arrayWithoutDuplicates;
        [weakSelf.table reloadData];
    } errorUsr:^{
        NSLog(@"error load data");
    }];
}

-(void)getLatestLoans
{
    __weak typeof(self) weakSelf = self;
    [Request reloadUser:@"http://www.grs.de/api/news-archive.jsonp" reply:^(id reply) {
        weakSelf.arrTable = reply;
        [weakSelf.table reloadData];
        
        [weakSelf.refreshControl endRefreshing];
    } errorUsr:^{
        NSLog(@"error load data");
    }];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arrNews"]];
    [arr addObject:self.arrTable[indexPath.row][@"nid"]];
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:@"arrNews"];
    
    NSLog(@"%@",self.arrTable[indexPath.row][@"nid"]);
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.table];
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:hitPoint];
    
    id cell = [self.table cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    FullNewsVC *vc = segue.destinationViewController;
    
    vc.strTitle = self.arrTable[indexPath.row][@"node_title"];
    vc.strText = self.arrTable[indexPath.row][@"body_1"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSString *dateString = self.arrTable[indexPath.row][@"Datum"][0];
    NSDate *dateTime = [dateFormat dateFromString:dateString];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
    vc.strDate = [dateFormat stringFromDate:dateTime];//self.arrTable[indexPath.row][@"Datum"][0];
}


@end
