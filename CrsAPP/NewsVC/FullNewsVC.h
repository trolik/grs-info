//
//  FullNewsVC.h
//  CrsAPP
//
//  Created by Сергей Лазаренко on 12/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullNewsVC : UIViewController

@property (nonatomic, strong) NSString *strDate;
@property (nonatomic,strong) NSString *strTitle;
@property (nonatomic,strong) NSString *strText;

@end
