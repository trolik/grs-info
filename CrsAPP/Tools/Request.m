//
//  request.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 11/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "request.h"

@implementation Request

+ (void)loadAllLinks:(void (^)(id reply))replyArr errorUsr:(void (^)())errorUsr
{
    NSMutableDictionary *arr = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"http://www.grs.de/api/news-archive.jsonp",@"News",
                                @"http://p280790.mittwaldserver.info/app/info/data/tweets_json.php?count=250&page=1",@"Tweets",nil];
//                                @"http://www.grs.de/api/events-archive.jsonp",@"events", nil];
    
    __block NSInteger mm = 0;

    for (int i=0;i<[[arr allKeys] count];i++)
    {
        [self reloadUser:arr[[arr allKeys][i]] reply:^(id reply) {
            [arr setObject:reply forKey:[arr allKeys][i]];
            mm++;
            if (mm == [[arr allKeys] count])
            {
                replyArr(arr);
            }
        } errorUsr:^{
            NSLog(@"error load");
        }];
    }
}


+(void)reloadUser:(NSString*)strUrl reply:(void (^)(id reply))reply errorUsr:(void (^)())errorUsr
{
    NSURLSessionConfiguration* configSession = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString: strUrl]];
    [request setValue:@"ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3" forHTTPHeaderField:@"Accept-Language"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                reply([NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]);
            });
            
        }
    }]resume];
}


+ (void) operationWihtRequestString:(NSString *)request сompletion:(void (^)(BOOL sucess, NSData *recievedData))completion
{
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 3;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    
    [[session dataTaskWithURL:[NSURL URLWithString:request] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          if (error)
          {
              NSLog(@"–––––\nError in request: %@\n%@\n–––––", request, error);
              [[NSOperationQueue mainQueue] addOperationWithBlock:^{completion(NO, nil);}];
              return;
          }
          
          if (!completion)
          {
              NSLog(@"Completion block was not set");
              return;
          }
          
          if (data)
          {
              [[NSOperationQueue mainQueue] addOperationWithBlock:^{completion(YES, data);}];
          }
      }]
     resume];
}


+ (void)sendToken:(NSString *)token replyUsr:(void (^)(id reply))replyUsr errorUsr:(void (^)())errorUsr
{
    [self reloadUser:[NSString stringWithFormat:@"http://p280790.mittwaldserver.info/app/info/api/?action=add_ios_token&token=%@",token] reply:^(id reply) {
        replyUsr(reply);
    } errorUsr:^{
        errorUsr();
    }];
}

@end
