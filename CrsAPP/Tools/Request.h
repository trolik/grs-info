//
//  request.h
//  CrsAPP
//
//  Created by Сергей Лазаренко on 11/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Request : NSObject

+ (void)loadAllLinks:(void (^)(id reply))replyArr errorUsr:(void (^)())errorUsr;
+(void)reloadUser:(NSString*)strUrl reply:(void (^)(id reply))reply errorUsr:(void (^)())errorUsr;
+ (void) operationWihtRequestString:(NSString *)request сompletion: (void (^)(BOOL sucess, NSData *recievedData)) sucess;

#pragma mark - push

+ (void)sendToken:(NSString *)token replyUsr:(void (^)(id reply))replyUsr errorUsr:(void (^)())errorUsr;

@end
