//
//  FullWebView.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 18/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "FullWebView.h"

@interface FullWebView ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation FullWebView

- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    [self.webView loadRequest:self.request];
}

- (IBAction)closeButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}



@end
