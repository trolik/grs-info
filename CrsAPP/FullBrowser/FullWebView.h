//
//  FullWebView.h
//  CrsAPP
//
//  Created by Сергей Лазаренко on 18/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullWebView : UIViewController

@property (nonatomic, strong) NSURLRequest *request;

@end
