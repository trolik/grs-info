//
//  CareerVC.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 13/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "CareerVC.h"
#import "Request.h"
#import "CareerCell.h"
#import "FullCareerVC.h"

@interface CareerVC () <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableCareer;
@property NSArray *arrTable;

@end

@implementation CareerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.navigationItem setLeftBarButtonItem:Item1];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, [UIImage imageNamed:@"barMenu"].size.width, [UIImage imageNamed:@"barMenu"].size.height);
    [btn setImage:[UIImage imageNamed:@"barMenu"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.navigationItem setRightBarButtonItem:Item];
    
    __weak typeof(self) weakSelf = self;
    [Request reloadUser:@"http://www.grs.de/api/kariere-archive.jsonp" reply:^(id reply) {
        weakSelf.arrTable = reply;
        [weakSelf.tableCareer reloadData];
    } errorUsr:^{
        NSLog(@"error load data");
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrTable count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CareerCell *cell = (CareerCell*)[tableView dequeueReusableCellWithIdentifier:@"CareerCell" forIndexPath:indexPath];

    [cell.labelTitle setText:self.arrTable[indexPath.row][@"node_title"]];
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableCareer];
    NSIndexPath *indexPath = [self.tableCareer indexPathForRowAtPoint:hitPoint];
    
    FullCareerVC *vc = segue.destinationViewController;
    
    vc.strText = self.arrTable[indexPath.row][@"body"];

}

-(void)clickMenu
{
    [self.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
