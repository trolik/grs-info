//
//  FullCareerVC.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 13/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "FullCareerVC.h"
#import "FullWebView.h"

@interface FullCareerVC () <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webViewCareers;

@end

@implementation FullCareerVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.hidesBackButton = YES;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, [UIImage imageNamed:@"logo"].size.width, [UIImage imageNamed:@"logo"].size.height);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    [self.navigationItem setLeftBarButtonItem:Item1];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, [UIImage imageNamed:@"barMenu"].size.width, [UIImage imageNamed:@"barMenu"].size.height);
    [btn setImage:[UIImage imageNamed:@"barMenu"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *Item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.navigationItem setRightBarButtonItem:Item];
    
    NSString *strPath = [[NSBundle mainBundle]pathForResource:@"css1" ofType:@"css"];
    
    NSString* htmlString = [NSString stringWithContentsOfFile:strPath encoding:NSUTF8StringEncoding error:nil];
    
    NSString *textHtml = [NSString stringWithFormat:@"<style>%@</style>", htmlString];
    textHtml = [textHtml stringByAppendingString:@"<script type=\"text/javascript\">$(\".app_nofollow\").each(function(){ var linkText = $(this).text();$(this).before(linkText);$(this).remove();});</script>"];
//    NSString *textHtml = @"<FONT COLOR=\"#4A617D\">";
//    textHtml = [textHtml stringByAppendingString:@"<style>a { color: red; !important } .table{    width: 100% !important;    font-size: 1em;    padding: 4px 10px 4px 5px !important;}.headlist thead tr th, .table thead tr th{    background-color: #f39200;    font-weight: bold !important;    padding: 10px 4px !important;    color: #fff;    font-size: 1.2em !important; }.headlist td, .table td{    padding: 5px 10px 5px 5px !important;    text-align: left;    color: #676767;}.table strong{    font-weight: bold !important;}.a{    color: #c17400 !important;}</style>"];
    textHtml = [textHtml stringByAppendingString:self.strText];
    textHtml = [textHtml stringByReplacingOccurrencesOfString:@"src=\"/" withString:@"src=\"http://www.grs.de/"];
    textHtml = [textHtml stringByReplacingOccurrencesOfString:@"href=\"/" withString:@"href=\"http://www.grs.de/"];
    [self.webViewCareers loadHTMLString:textHtml baseURL:nil];
}

-(void)clickMenu
{
    [self.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *reqUrl = [NSString stringWithFormat: @"%@",request.URL];
    if ([reqUrl rangeOfString:@"mailto"].location != NSNotFound)
    {
        return YES;
    }
    else if ([reqUrl rangeOfString:@"about:blank"].location == NSNotFound)
    {
        [self performSegueWithIdentifier:@"openFullBrowser" sender:request];
        
        return NO;
    }
    else
        return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    FullWebView *vc = [segue destinationViewController];
    vc.request = sender;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
