//
//  CellCollection.m
//  CrsAPP
//
//  Created by Сергей Лазаренко on 11/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import "CellCollection.h"

@implementation CellCollection

-(void)awakeFromNib
{
    [self.numView.layer setCornerRadius:15];
    [self.numView.layer setBorderWidth:1];
    [self.numView.layer setBorderColor:[UIColor blackColor].CGColor];
}


@end
