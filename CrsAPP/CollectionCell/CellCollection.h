//
//  CellCollection.h
//  CrsAPP
//
//  Created by Сергей Лазаренко on 11/11/15.
//  Copyright © 2015 Сергей Лазаренко. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellCollection : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgCell;
@property (strong, nonatomic) IBOutlet UILabel *labelCell;
@property (strong, nonatomic) IBOutlet UIView *viewCell;

@property (strong, nonatomic) IBOutlet UIView *numView;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;


@end
